package ar.com.turnos.oauth.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.security.oauth2.provider.token.store.JwtTokenStore;
@RefreshScope // para actualizar el objeto Environment sin reiniciar la aplicacion
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private AuthenticationManager authenticationManager;
	@Autowired
	private Environment environment;
	/*
	 * Este metodo configura los permisos del endpoint del servidor oauth2 para generar y validar el token
	 */
	@Override
	public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
		security.tokenKeyAccess("permitAll()") // permitimos a todos acceder al endpoint para generar el token "/oauth/token"
		.checkTokenAccess("isAuthenticated()"); // para validar el token debe estar autenticado. Ambas son funciones de spring security
	}
	/*
	 * Vamos a registrar los clientes que se van a comunicar con el back para loguearse.
	 */
	@Override
	public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
		clients.inMemory()
		.withClient(this.environment.getProperty("config.security.oauth.client.id")) // credencial del front
		.secret(this.passwordEncoder.encode(this.environment.getProperty("config.security.oauth.client.secret"))) // credencial del front
		.scopes("read","write") // permisos
		.authorizedGrantTypes("password","refresh_token") // credencial usario para authenticarse, en este caso password
		.accessTokenValiditySeconds(3600) //durante 1  hora es valido el token
		.refreshTokenValiditySeconds(3600); // despues de 1 hora se actualiza el token
		
	}
	/*
	 *- registra,ps el authentication manager, que agregamos como bean en  la clase SpringSecurityConfig.
	 *- token store creara el token y lo almacenara
	 *- por ultimo le definimos que vamos a usar un JWT, seteandoselo.
	 * -el endpoint es de el servidor de autenticacion "/oauth/token"
	 */

	@Override
	public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
		endpoints.authenticationManager(authenticationManager)
		.tokenStore(tokenStore())
		.accessTokenConverter(accessTokenConverter());
	}
	
	@Bean
	public JwtTokenStore tokenStore(){
		return new JwtTokenStore(this.accessTokenConverter());
	}
	
	/*
	 * Este metodo setea en el token la firma secreta para despues hacer validaciones
	 */
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {
		JwtAccessTokenConverter token = new JwtAccessTokenConverter();
		token.setSigningKey(this.environment.getProperty("config.security.oauth.jwt.key"));
		return token;
	}

	
}
