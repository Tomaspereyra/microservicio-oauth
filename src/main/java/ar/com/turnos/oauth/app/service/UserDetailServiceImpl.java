package ar.com.turnos.oauth.app.service;

import java.util.List;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ar.com.turnos.commons.usuarios.app.model.Usuario;
import ar.com.turnos.oauth.app.clients.IUsuarioFeignClient;
import feign.FeignException;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
	@Autowired
	private IUsuarioFeignClient clienteRest;
	
	private Logger log = LoggerFactory.getLogger(UserDetailServiceImpl.class);
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		try {
		Usuario usuario = clienteRest.findByUsername(username);
		
		List<GrantedAuthority> roles = usuario.getRoles()
				.stream().map(rol-> new SimpleGrantedAuthority(rol.getNombre())).collect(Collectors.toList());
		log.info("Usuario autenticado: "+username);
		return new User(usuario.getUsername(),usuario.getPassword(),usuario.getEnabled(),true,true,true,roles);

		}catch(FeignException e)
		{
			log.error("Error, no existe el usuario"+username+".");
			throw new UsernameNotFoundException("Error, no existe el usuario"+username+".");
		
		}
	}

}
