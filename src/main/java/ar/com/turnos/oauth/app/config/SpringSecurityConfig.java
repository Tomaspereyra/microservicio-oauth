package ar.com.turnos.oauth.app.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationEventPublisher;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import ar.com.turnos.oauth.app.service.UserDetailServiceImpl;

@Configuration
public class SpringSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private UserDetailServiceImpl userDetail;
	@Autowired
	private AuthenticationEventPublisher authenticationEventPublisher;
	/*
	 * Este metodo registra nuestra implementacion de UserDetailService en
	 * el AuthenticationManagerBuilder y tambien le definimos el algoritmo que vamos a usar para encriptar
	 * las claves. 
	 */
	
	@Override
	@Autowired
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(this.userDetail).passwordEncoder(passwordEncoder())
		.and().authenticationEventPublisher(this.authenticationEventPublisher);
	}
	/*
	 * Lo definimos con bean para registrarlo en spring y usarlo en otro lugar.
	 */
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	@Bean
	protected AuthenticationManager authenticationManager() throws Exception {
		// TODO Auto-generated method stub
		return super.authenticationManager();
	}
	
	
}
