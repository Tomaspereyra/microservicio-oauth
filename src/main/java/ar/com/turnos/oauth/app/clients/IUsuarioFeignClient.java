package ar.com.turnos.oauth.app.clients;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ar.com.turnos.commons.usuarios.app.model.Usuario;

@FeignClient(name="servicio-usuarios")
public interface IUsuarioFeignClient {

		@GetMapping("/usuarios/search/usuario")
		public Usuario findByUsername(@RequestParam String nombre);
}
